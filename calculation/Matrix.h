
#ifndef HOMOMORPHIC_ENCRYPTION_ARITHMETICS_MATRIX_H
#define HOMOMORPHIC_ENCRYPTION_ARITHMETICS_MATRIX_H

#include <vector>
#include <ostream>
#include "Vector.h"

class Matrix {
public:
    Matrix();
    explicit Matrix(const vector<Vector>& rows);

    const Vector& operator[](uint index) const;
    const Vector  operator()(uint index) const;

    const Matrix  operator+(const Matrix& another) const;
    const Matrix  operator-(const Matrix& another) const;
    const Matrix  operator*(const number& another) const;
    const Vector  operator*(const Vector& another) const;
    const Matrix  operator*(const Matrix& another) const;

    const number  det();
    const Matrix  getMinor(uint i, uint j);

    const Matrix  transpose() const;
protected:

private:
    vector<Vector> rows;
public:
    uint getHeight() const;

    uint getWidth() const;

    friend ostream &operator<<(ostream &os, const Matrix &matrix);

private:
    uint height;
    uint width;
};


#endif //HOMOMORPHIC_ENCRYPTION_ARITHMETICS_MATRIX_H
