
#ifndef HOMOMORPHIC_ENCRYPTION_ARITHMETICS_CALCULATION_H
#define HOMOMORPHIC_ENCRYPTION_ARITHMETICS_CALCULATION_H


#include "Vector.h"
#include "Matrix.h"

class Calculation {
public:
    static const Vector getOrthogonalVector(const Vector& v, uint degree);
    static const Vector getOrthogonalVector(const Vector& v);
    static const Matrix getOrthogonalMatrix(const Vector& v, uint height, uint degree);
    static const Matrix getOrthogonalMatrix(const Vector& v, uint height);
    static const Matrix getUnaryMatrix(uint size, uint degree);
    static const Matrix getUnaryMatrix(uint size);
    static const Matrix getUpperTriangleUnaryMatrix(uint size);
    static const Matrix getLowerTriangleUnaryMatrix(uint size);
protected:

private:
    static uint DEFAULT_DEGREE;
};


#endif //HOMOMORPHIC_ENCRYPTION_ARITHMETICS_CALCULATION_H
