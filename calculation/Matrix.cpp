
#include <iostream>
#include "Matrix.h"

Matrix::Matrix(const vector<Vector> &rows) {
    this->rows = rows;
    this->height = static_cast<uint>(rows.size());
    this->width = rows[0].getSize();
}

const Vector &Matrix::operator[](uint index) const {
    return rows[index];
}

const Vector Matrix::operator()(uint index) const {
    auto result = vector<number>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = (rows[i])[index];
    }
    return Vector(result);
}

const Matrix Matrix::operator+(const Matrix &another) const {
    auto result = vector<Vector>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = rows[i] + another[i];
    }
    return Matrix(result);
}

const Matrix Matrix::operator-(const Matrix &another) const {
    auto result = vector<Vector>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = rows[i] - another[i];
    }
    return Matrix(result);
}

const Matrix Matrix::operator*(const number &another) const {
    auto result = vector<Vector>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = rows[i] * another;
    }
    return Matrix(result);
}

const Vector Matrix::operator*(const Vector &another) const {
    auto result = vector<number>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = rows[i] * another;
    }
    return Vector(result);
}

const Matrix Matrix::operator*(const Matrix &another) const {
    auto size = another.width;
    auto result = vector<Vector>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = (*this) * another(i);
    }
    return Matrix(result).transpose();
}

const Matrix Matrix::transpose() const {
    auto result = vector<Vector>(width);
    for (uint i = 0; i < width; i++) {
        result[i] = (*this)(i);
    }
    return Matrix(result);
}

uint Matrix::getHeight() const {
    return height;
}

uint Matrix::getWidth() const {
    return width;
}

const number Matrix::det() {
    if (width != height) {
        return 0;
    }
    if (width == height && height == 1) {
        return rows[0][0];
    }
    number result = 0;
    uint i = 0;
    for (uint j = 0; j < height; j++) {
        Matrix m = getMinor(i, j);
        number k = (((i + j) % 2) == 0) ? 1 : -1;
        number A = m.det() * k;
        result = result + (A * (rows[i])[j]);
    }
    return result;
}

const Matrix Matrix::getMinor(uint i, uint j) {
    auto result = vector<Vector>();
    for (uint row_index = 0; row_index < height; row_index++) {
        if (row_index == i) continue;
        auto row = vector<number>();
        for (uint column_index = 0; column_index < width; column_index++) {
            if (column_index == j) continue;
            row.emplace_back((rows[row_index])[column_index]);
        }
        result.emplace_back(Vector(row));
    }
    return Matrix(result);
}

ostream &operator<<(ostream &os, const Matrix &matrix) {
    for (uint i = 0; i < matrix.height; i++) {
        for (uint j = 0; j < matrix.width; j++) {
            os << (matrix[i])[j].get_str(10) << " ";
        }
        os << std::endl;
    }
    return os;
}

Matrix::Matrix() {
    this->rows = vector<Vector>();
    this->height = 0;
    this->width = 0;
}


