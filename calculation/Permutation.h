
#ifndef HOMOMORPHIC_ENCRYPTION_ARITHMETICS_PERMUTATION_H
#define HOMOMORPHIC_ENCRYPTION_ARITHMETICS_PERMUTATION_H

#include <vector>
#include <ntsid.h>
#include "Vector.h"
#include "Matrix.h"

class Permutation {
public:
    explicit Permutation(vector<uint>& content);

    uint          operator[]  (uint index) const;

    const Vector        operator*   (const Vector& v) const;
    const Matrix        operator*   (const Matrix& m) const;
    const Permutation   operator*   (const Permutation& p) const;
protected:

private:
    uint size;
    vector<uint> content;
};


#endif //HOMOMORPHIC_ENCRYPTION_ARITHMETICS_PERMUTATION_H
