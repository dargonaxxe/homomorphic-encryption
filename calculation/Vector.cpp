
#include "Vector.h"
#include <vector>
#include <iostream>

Vector::Vector() {
    this->size = 0;
    this->content = vector<number>();
}

inline const number& Vector::operator[](uint index) const {
    return content[index];
}

const Vector Vector::operator+(const Vector &another) const {
    auto result = vector<number>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = (*this)[i] + another[i];
    }
    return Vector(result);
}

const Vector Vector::operator-(const Vector &another) const {
    auto result = vector<number>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = (*this)[i] - another[i];
    }
    return Vector(result);
}

const Vector Vector::operator*(const number &another) const {
    auto result = vector<number>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = (*this)[i] * another;
    }
    return Vector(result);
}

const number Vector::operator*(const Vector &another) const {
    number result = 0;
    for (uint i = 0; i < size; i++) {
        result = result + ((*this)[i] * another[i]);
    }
    return result;
}

Vector::Vector(const vector<number> &content) {
    this->size = static_cast<uint>(content.size());
    this->content = content;
}

uint Vector::getSize() const {
    return size;
}





