
#include "Permutation.h"

Permutation::Permutation(vector<uint> &content) {
    this->size = static_cast<uint>(content.size());
    this->content = content;
}

uint Permutation::operator[](uint index) const {
    return content[index];
}

// TODO: implement tests for this method
const Vector Permutation::operator*(const Vector &v) const {
    auto result = vector<number>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = v[content[i]];
    }
    return Vector(result);
}

// TODO: implement tests for this method
const Matrix Permutation::operator*(const Matrix &m) const {
    auto result = vector<Vector>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = m[content[i]];
    }
    return Matrix(result);
}

// TODO: implement tests for this method
const Permutation Permutation::operator*(const Permutation &p) const {
    auto result = vector<uint>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = p[content[i]];
    }
    return Permutation(result);
}

