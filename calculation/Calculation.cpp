
#include "Calculation.h"
#include "../factory/NumberFactory.h"

uint Calculation::DEFAULT_DEGREE = 4;

const Vector Calculation::getOrthogonalVector(const Vector &v, uint degree) {
    auto size = v.getSize();
    auto result = vector<number>(size);
    for (uint j = 0; j < degree; j++) {
        for (uint i = 0; i < size; i++) {
            auto k = NumberFactory::getRandomIndex(size);
            auto c = NumberFactory::getRandomNumber();
            result[i] += v[k] * c;
            result[k] -= v[i] * c;
        }
    }
    return Vector(result);
}

const Vector Calculation::getOrthogonalVector(const Vector &v) {
    return getOrthogonalVector(v, DEFAULT_DEGREE);
}

const Matrix Calculation::getOrthogonalMatrix(const Vector &v, uint height, uint degree) {
    auto result = vector<Vector>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = getOrthogonalVector(v, degree);
    }
    return Matrix(result);
}

const Matrix Calculation::getOrthogonalMatrix(const Vector &v, uint height) {
    return getOrthogonalMatrix(v, height, DEFAULT_DEGREE);
}

// Todo: Implement tests for this method
const Matrix Calculation::getUnaryMatrix(uint size) {
    auto l = getLowerTriangleUnaryMatrix(size);
    auto u = getUpperTriangleUnaryMatrix(size);
    return l * u;
}

// det A = \sum_{i=1}^{n} a_{ij}A_{ij}
// a_{ij} -- matrix element
// A_{ij} = (-1)^{i+j} * M_{ij}

// Todo: Implement tests for this method
const Matrix Calculation::getUpperTriangleUnaryMatrix(uint size) {
    auto result = vector<Vector>(size);
    for (uint i = 0; i < size; i++) {
        uint row_elements = size - i;
        auto row = NumberFactory::getRandomStdVector(row_elements);
        for (uint j = 0; j < (size - row_elements); j++) {
            row.emplace(row.begin(), 0);
        }
        row[i] = 1;
        result[i] = Vector(row);
    }
    return Matrix(result);
}

// Todo: implement tests for this method
const Matrix Calculation::getLowerTriangleUnaryMatrix(uint size) {
    auto result = vector<Vector>(size);
    for (uint i = 0; i < size; i++) {
        uint row_elements = i + 1;
        auto row = NumberFactory::getRandomStdVector(row_elements);
        for (uint j = 0; j < (size - row_elements); j++) {
            row.emplace_back(0);
        }
        row[i] = 1;
        result[i] = Vector(row);
    }
    return Matrix(result);
}

// Todo: implement tests for this method
const Matrix Calculation::getUnaryMatrix(uint size, uint degree) {
    auto result = getUnaryMatrix(size);
    for (uint i = 1; i < degree; i++) {
        result = (result * getUnaryMatrix(size)).transpose();
    }
    return result;
}



