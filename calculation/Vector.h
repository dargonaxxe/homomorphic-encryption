
#ifndef HOMOMORPHIC_ENCRYPTION_ARITHMETICS_VECTOR_H
#define HOMOMORPHIC_ENCRYPTION_ARITHMETICS_VECTOR_H


#include <zconf.h>
#include <gmpxx.h>
#include <vector>

typedef mpz_class number;

using namespace std;

class Vector {
public:
    Vector();
    explicit Vector(const vector<number> &content);

    uint getSize() const;

    const number& operator[](uint index) const;
    const Vector operator+(const Vector& another) const;
    const Vector operator-(const Vector& another) const;
    const Vector operator*(const number& another) const;
    const number operator*(const Vector& another) const;
protected:

private:
    uint size;
    vector<number> content;
};


#endif //HOMOMORPHIC_ENCRYPTION_ARITHMETICS_VECTOR_H
