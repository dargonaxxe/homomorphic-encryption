
#include "NumberFactory.h"
#include <cstdlib>

uint NumberFactory::DEFAULT_NUMBER_BIT_LENGTH = 512;
gmp_randclass NumberFactory::random = gmp_randinit_default;

const number NumberFactory::getRandomNumber(uint bit_length) {
    return random.get_z_bits(bit_length);
}

const number NumberFactory::getRandomNumber() {
    return getRandomNumber(DEFAULT_NUMBER_BIT_LENGTH);
}

const Matrix NumberFactory::getRandomMatrix(uint height, uint width) {
    auto result = vector<Vector>(height);
    for (uint i = 0; i < height; i++) {
        result[i] = getRandomVector(width);
    }
    return Matrix(result);
}

const vector<number> NumberFactory::getRandomStdVector(uint size) {
    return getRandomStdVector(size, DEFAULT_NUMBER_BIT_LENGTH);
}

// todo: implement tests for this method
const vector<number> NumberFactory::getRandomStdVector(uint size, uint bit_length) {
    auto result = vector<number>(size);
    for (uint i = 0; i < size; i++) {
        result[i] = getRandomNumber(bit_length);
    }
    return result;
}

const Vector NumberFactory::getRandomVector(uint size) {
    return getRandomVector(size, DEFAULT_NUMBER_BIT_LENGTH);
}

const Vector NumberFactory::getRandomVector(uint size, uint bit_length) {
    return Vector(getRandomStdVector(size, bit_length));
}
// Todo: implement tests for this method
const Permutation NumberFactory::getRandomPermutation(uint size) {
    auto tmp = vector<uint>();
    for (uint i = 0; i < size; i++) {
        tmp.emplace_back(i);
    }
    auto result = vector<uint>();
    for (uint i = 0; i < size; i++) {
        auto index = getRandomIndex(static_cast<uint>(tmp.size()));
        result.emplace_back(tmp[index]);
        tmp.erase(tmp.begin() + index);
    }
    return Permutation(result);
}
