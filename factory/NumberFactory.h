//
// Created by Кузьмин Илья on 15/08/2018.
//

#ifndef HOMOMORPHIC_ENCRYPTION_ARITHMETICS_NUMBERFACTORY_H
#define HOMOMORPHIC_ENCRYPTION_ARITHMETICS_NUMBERFACTORY_H


#include "../calculation/Vector.h"
#include "../calculation/Matrix.h"
#include "../calculation/Permutation.h"

class NumberFactory {
public:
    static const number getRandomNumber(uint bit_length);
    static const number getRandomNumber();
    static const Vector getRandomVector(uint size);
    static const Vector getRandomVector(uint size, uint bit_length);
    static const Matrix getRandomMatrix(uint height, uint width);
    static const vector<number> getRandomStdVector(uint size);
    static const vector<number> getRandomStdVector(uint size, uint bit_length);
    static const Permutation getRandomPermutation(uint size);
    static inline uint getRandomIndex(uint border) {
        return ((uint) rand()) % border;
    };
protected:

private:
    static uint DEFAULT_NUMBER_BIT_LENGTH;
    static gmp_randclass random;
};


#endif //HOMOMORPHIC_ENCRYPTION_ARITHMETICS_NUMBERFACTORY_H
