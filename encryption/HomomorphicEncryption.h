#ifndef HOMOMORPHIC_ENCRYPTION_ARITHMETICS_HOMOMORPHICENCRYPTION_H
#define HOMOMORPHIC_ENCRYPTION_ARITHMETICS_HOMOMORPHICENCRYPTION_H


#include "../calculation/Vector.h"
#include "../calculation/Matrix.h"

class HomomorphicEncryption {
public:
    explicit HomomorphicEncryption(const Vector& key);

    const Vector encrypt(const number& value) const;
    const Matrix encrypt(const Vector& value) const;
    const number decrypt(const Vector& value) const;
    const Vector decrypt(const Matrix& value) const;

    void shuffle();
protected:

private:
    Vector key;
    uint size;
    Matrix orth_vectors;
};


#endif //HOMOMORPHIC_ENCRYPTION_ARITHMETICS_HOMOMORPHICENCRYPTION_H
