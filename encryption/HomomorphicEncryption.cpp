#include "HomomorphicEncryption.h"
#include "../calculation/Calculation.h"
#include "../factory/NumberFactory.h"

HomomorphicEncryption::HomomorphicEncryption(const Vector &key) {
    this->size = key.getSize();
    auto coordinates = vector<number>(size);
    for (uint i = 0; i < size; i++) {
        number t = key[i];
        coordinates[i] = t;
    }
    coordinates[0] = 1;
    this->key = Vector(coordinates);
    shuffle();
}

const Vector HomomorphicEncryption::encrypt(const number &value) const {
    auto container = vector<number>(size);
    container[0] = value;
    auto gamma = NumberFactory::getRandomVector(orth_vectors.getWidth());
    auto result = Vector(container) + (orth_vectors * gamma);
    return result;
}

const Matrix HomomorphicEncryption::encrypt(const Vector &value) const {
    uint height = value.getSize();
    auto container = vector<Vector>(height);
    for (uint i = 0; i < height; i++) {
        container[i] = encrypt(value[i]);
    }
    return Matrix(container);
}

const number HomomorphicEncryption::decrypt(const Vector &value) const {
    return value * key;
}

const Vector HomomorphicEncryption::decrypt(const Matrix &value) const {
    return value * key;
}

void HomomorphicEncryption::shuffle() {
    this->orth_vectors = Calculation::getOrthogonalMatrix(key, size / 2).transpose();
}


