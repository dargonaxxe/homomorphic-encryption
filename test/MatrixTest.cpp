
#include <gtest/gtest.h>
#include "../calculation/Vector.h"
#include "../calculation/Matrix.h"
#include <vector>

TEST(matrix_constructor, basic) {
    Matrix a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    ASSERT_EQ(a.getWidth(), 3);
    ASSERT_EQ(a.getHeight(), 3);
}

TEST(matrix_access_operations, get_row) {
    Matrix a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    ASSERT_EQ(a[0][0], 1);
    ASSERT_EQ(a[1][0], 4);
    ASSERT_EQ(a[2][0], 7);
}

TEST(matrix_access_operations, get_column) {
    Matrix a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    for (uint i = 0; i < a.getHeight(); i++) {
        for (uint j = 0; j < a.getWidth(); j++) {
            ASSERT_EQ(a[i][j], a(j)[i]);
        }
    }
}

TEST(matrix_arithmetics_operations, plus_matrix) {
    Matrix a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    Matrix b = Matrix({
        Vector({ 9, 8, 7 }),
        Vector({ 6, 5, 4 }),
        Vector({ 3, 2, 1 })
    });
    Matrix c = a + b;
    uint width = c.getWidth();
    uint height = c.getHeight();
    ASSERT_EQ(width, 3);
    ASSERT_EQ(height, 3);
    for (uint i = 0; i < height; i++) {
        for (uint j = 0; j < width; j++) {
            ASSERT_EQ(c[i][j], 10);
        }
    }
}

TEST(matrix_arithmetics_operations, minus_matrix) {
    Matrix a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    Matrix b = Matrix({
        Vector({ 3, 4, 5 }),
        Vector({ 6, 7, 8 }),
        Vector({ 9, 10, 11 })
    });
    Matrix c = b - a;
    uint width = c.getWidth();
    uint height = c.getHeight();
    ASSERT_EQ(width, 3);
    ASSERT_EQ(height, 3);
    for (uint i = 0; i < height; i++) {
        for (uint j = 0; j < width; j++) {
            ASSERT_EQ(c[i][j], 2);
        }
    }
}

TEST(matrix_arithmetics_operations, matrix_times_number) {
    Matrix a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    number k = 9;
    Matrix b = a * k;
    uint width = b.getWidth();
    uint height = b.getHeight();
    ASSERT_EQ(width, 3);
    ASSERT_EQ(height, 3);
    for (uint i = 0; i < height; i++) {
        for (uint j = 0; j < width; j++) {
            ASSERT_EQ(b[i][j], a[i][j] * k);
        }
    }
}

TEST(matrix_arithmetics_operations, matrix_times_vector) {
    auto a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    auto b = Vector({ 1, 1, 1 });
    auto c = a * b;
    ASSERT_EQ(c.getSize(), 3);
    ASSERT_EQ(c[0], 6);
    ASSERT_EQ(c[1], 15);
    ASSERT_EQ(c[2], 24);
}

TEST(matrix_arithmetics_operations, matrix_times_unary_matrix) {
    auto a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    auto b = Matrix({
        Vector({ 1, 0, 0 }),
        Vector({ 0, 1, 0 }),
        Vector({ 0, 0, 1 })
    });
    auto c = a * b;
    auto d = b * a;
    ASSERT_EQ(c.getWidth(), d.getWidth());
    ASSERT_EQ(c.getHeight(), d.getHeight());
    ASSERT_EQ(c.getHeight(), c.getWidth());
    ASSERT_EQ(c.getWidth(), 3);
    ASSERT_EQ(c[1][1], d[1][1]);
    ASSERT_EQ(d[1][1], a[1][1]);
}

TEST(matrix_arithmetics_operations, matrix_times_k_matrix) {
    auto a = Matrix({
        Vector({ 1, 2, 3 }),
        Vector({ 4, 5, 6 }),
        Vector({ 7, 8, 9 })
    });
    uint k = 4;
    auto b = Matrix({
        Vector({ k, 0, 0 }),
        Vector({ 0, k, 0 }),
        Vector({ 0, 0, k })
    });
    auto c = a * b;
    uint width = c.getWidth();
    uint height = c.getHeight();
    ASSERT_EQ(width, 3);
    ASSERT_EQ(height, 3);
    for (uint i = 0; i < height; i++) {
        for (uint j = 0; j < width; j++) {
            ASSERT_EQ(c[i][j], a[i][j] * k);
        }
    }
}

TEST(matrix_arithmetics_operations, determinant) {
    Matrix a = Matrix(vector<Vector>({
        Vector({ 1, 0, 0 }),
        Vector({ 0, 1, 0 }),
        Vector({ 0, 0, 1 })
    }));
    number d = a.det();
    ASSERT_EQ(d, 1);
}

TEST(matrix_arithmetics_operations, determinant_zero) {
    Matrix a = Matrix(vector<Vector>({
        Vector({ 2, 4, 6 }),
        Vector({ 2, 4, 6 }),
        Vector({ 2, 4, 6 })
    }));
    number d = a.det();
    ASSERT_EQ(d, 0);
}

TEST(matrix_arithmetics_operations, determinant_k) {
    uint k = 147;
    Matrix a = Matrix(vector<Vector>({
        Vector({ k, 0, 0 }),
        Vector({ 0, k, 0 }),
        Vector({ 0, 0, k })
    }));
    number d = a.det();
    ASSERT_EQ(d, k * k * k);
}