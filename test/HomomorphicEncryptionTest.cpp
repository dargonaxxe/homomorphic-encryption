

#include <gtest/gtest.h>
#include "../factory/NumberFactory.h"
#include "../encryption/HomomorphicEncryption.h"

TEST(encryption_test, homomorphic_encryption_number) {
    uint length = 40;
    auto a = NumberFactory::getRandomNumber();
    auto k = NumberFactory::getRandomVector(length);
    auto encryption = HomomorphicEncryption(k);
    auto e = encryption.encrypt(a);
    auto d = encryption.decrypt(e);
    ASSERT_EQ(d, a);
}

TEST(encryption_test, homomorphic_encryption_vector) {
    uint length = 20;
    uint vector_length = 250;
    auto a = NumberFactory::getRandomVector(vector_length);
    auto k = NumberFactory::getRandomVector(length);
    auto encryption = HomomorphicEncryption(k);
    auto e = encryption.encrypt(a);
    auto d = encryption.decrypt(e);
    for (uint i = 0; i < vector_length; i++) {
        ASSERT_EQ(a[i], d[i]);
    }
}