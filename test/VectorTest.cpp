
#include <gtest/gtest.h>
#include "../calculation/Vector.h"
#include <vector>

TEST(vector_constructor_test, std_vector) {
    auto v = Vector(vector<number>({ 1, 2, 3, 4 }));
    ASSERT_EQ(v[0], 1);
    ASSERT_EQ(v[1], 2);
    ASSERT_EQ(v[2], 3);
    ASSERT_EQ(v[3], 4);
    ASSERT_EQ(v.getSize(), 4);
}

TEST(vector_operations_test, add) {
    auto v1 = Vector(vector<number>{ 1, 2, 3, 4 });
    auto v2 = Vector(vector<number>{ 4, 3, 2, 1 });
    auto v3 = v1 + v2;
    for (uint i = 0; i < 4; i++) {
        ASSERT_EQ(v3[i], 5);
        ASSERT_EQ(v3[i], v2[i] + v1[i]);
    }
    ASSERT_EQ(v3.getSize(), v2.getSize());
    ASSERT_EQ(v2.getSize(), v1.getSize());
}

TEST(vector_operations_test, subtract) {
    auto v1 = Vector(vector<number> { 1, 2, 3, 4 });
    auto v2 = Vector(vector<number> { 5, 6, 7, 8 });
    auto v3 = v2 - v1;
    for (uint i = 0; i < v3.getSize(); i++) {
        ASSERT_EQ(v3[i], 4);
        ASSERT_EQ(v3[i], v2[i] - v1[i]);
    }
    ASSERT_EQ(v3.getSize(), v2.getSize());
    ASSERT_EQ(v2.getSize(), v1.getSize());
}

TEST(vector_operations_test, times_by_number) {
    auto v1 = Vector(vector<number> { 1, 2, 3, 4 });
    number k = 10;
    auto v2 = v1 * k;
    for (uint i = 0; i < v2.getSize(); i++) {
        ASSERT_EQ(v2[i], v1[i] * k);
    }
    ASSERT_EQ(v1.getSize(), v2.getSize());
}

TEST(vector_operations_test, dot_product) {
    auto v1 = Vector(vector<number> { 1, 2, 3, 4 });
    auto v2 = Vector(vector<number> { 4, 3, 2, 1 });
    number result = v1 * v2;
    ASSERT_EQ(result, 4 + 6 + 6 + 4);
}

