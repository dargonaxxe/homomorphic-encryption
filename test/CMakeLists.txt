
project(arithmetics_tests)

include_directories(${gtest_source_dir}/include ${gtest_source_dir})

set(vector_test_source_files VectorTest.cpp)
set(permutation_test_source_files PermutationTest.cpp)
set(matrix_test_source_files MatrixTest.cpp)
set(factory_test_source_files NumberFactoryTest.cpp)
set(calculation_test_source_files CalculationTest.cpp)
set(encryption_test_source_files HomomorphicEncryptionTest.cpp)
set(source_files ${vector_test_source_files} ${permutation_test_source_files} ${matrix_test_source_files}
        ${factory_test_source_files} ${calculation_test_source_files} ${encryption_test_source_files})

add_subdirectory(lib/googletest)

add_executable(whole_test ${source_files})
target_link_libraries(whole_test calculation)
target_link_libraries(whole_test gtest gtest_main)
target_link_libraries(whole_test gmp gmpxx)
