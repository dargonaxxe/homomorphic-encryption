
#include <gtest/gtest.h>
#include "../factory/NumberFactory.h"
#include "../calculation/Calculation.h"

TEST(calculation_test, orth_vector) {
    uint size = 64;
    uint attempts = 1024;
    auto a = NumberFactory::getRandomVector(size);
    for (uint i = 0; i < attempts; i++) {
        auto b = Calculation::getOrthogonalVector(a);
        ASSERT_EQ(b * a, 0);
    }
}

TEST(calculation_test, orth_matrix) {
    uint width = 128;
    uint height = 64;
    uint attempts = 64;
    auto a = NumberFactory::getRandomVector(width);
    for (uint i = 0; i < attempts; i++) {
        auto m = Calculation::getOrthogonalMatrix(a, height);
        auto r = m * a;
        ASSERT_EQ(r.getSize(), height);
        for (uint j = 0; j < r.getSize(); j++) {
            ASSERT_EQ(r[j], 0);
        }
    }
}

TEST(calculation_test, unary_matrix) {
    auto m = Calculation::getUnaryMatrix(9, 1);
    auto d = m.det();
    ASSERT_EQ(d, 1);
}