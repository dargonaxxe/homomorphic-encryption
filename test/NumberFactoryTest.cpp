
#include <gtest/gtest.h>
#include "../calculation/Vector.h"
#include "../factory/NumberFactory.h"

TEST(number_factory_test, number_fixed_size) {
    uint length = 128;
    number a = NumberFactory::getRandomNumber(length);
    uint pow = 1;
    while (a > 0) {
        a = a / 2;
        pow++;
    }
    ASSERT_GE(pow, length / 2);
}

TEST(number_factory_test, vector) {
    uint size = 256;
    auto a = NumberFactory::getRandomVector(size);
    ASSERT_EQ(a.getSize(), size);
}

TEST(number_factory_test, matrix) {
    uint width = 512;
    uint height = 512;
    auto a = NumberFactory::getRandomMatrix(height, width);
    ASSERT_EQ(a.getWidth(), width);
    ASSERT_EQ(a.getHeight(), height);
}

TEST(number_factory_test, uint) {
    uint border = 512;
    uint attempts = 1024;
    for (uint i = 0; i < attempts; i++) {
        auto value = NumberFactory::getRandomIndex(border);
        ASSERT_GE(border, value);
        ASSERT_GE(value, 0);
    }
}